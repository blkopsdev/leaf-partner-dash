import compact from 'lodash/compact';
import isEmpty from 'lodash/isEmpty';
import React, { PropTypes, Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import Promise from 'bluebird';
import ReactTooltip from 'react-tooltip';

import { fetchEventTypes } from '../../../actions/EventTypeActions';
import { fetchLocations } from '../../../actions/LocationActions';
import { fetchSpecials } from '../../../actions/SpecialActions';

import {
  LinkTo,
  DateNameStartEndList,
  TextCheckboxField,
  EventbriteFinder,
  renderTextareaField,
  renderDropdownList,
  renderDateTimePicker,
  renderCheckboxField,
  renderTooltip,
  renderField,
} from '../../../helpers';

class EventForm extends Component {

  state = {};

  componentDidMount() {
    const { currentUser, fetchEventTypes, fetchLocations, fetchSpecials } = this.props;

    Promise.all([
      fetchEventTypes({}),
      fetchLocations({}),
      fetchSpecials({}, currentUser)
    ]).then(() => this.handleInitialize());
  }

  handleInitialize() {
    const {
      item,
      item: {
        event_type, dates, start_time, end_time, activity, location, description, redemption, eventbrite, cost,
        add_criteria, gender, age, comments_for_reviewer, boost_status, special
      },
      initialize
    } = this.props;

    if (!isEmpty(item)) {
      this.setState({ add_criteria, redemption: redemption ? redemption.value : null }, () => initialize({
        event_type, dates, start_time, end_time, activity, location, description, redemption, eventbrite, cost,
        add_criteria, gender, age, comments_for_reviewer, boost_status, special
      }));
    }
  }

  render () {
    const { item, eventTypes, locations, specials, errorMessage, handleSubmit, onSave } = this.props;
    const { add_criteria, redemption } = this.state;

    return (
      <form onSubmit={handleSubmit(event => {onSave(event)})}>
        <div className="row">
          <div className="col-md-8">
            <Field
              name="event_type"
              valueField="objectId"
              textField="name"
              component={renderDropdownList}
              data={eventTypes.map(({ objectId, name }) => ({ objectId, name }))}
              label="Event Type"
              placeholder="Select Event Type"
            />
            <Field
              name="dates"
              component={DateNameStartEndList}
              label="Event Name and Dates"
              placeholder="Event Name"
            />
            <Field 
              name="description" 
              component={renderTextareaField} 
              label="Event Description"
              placeholder="Example: Live jazz music from Brooklyn based trio, Smarty Pants, featuring Josh Yoken"
            />
            <Field
              name="redemption"
              valueField="value"
              textField="name"
              component={renderDropdownList}
              data={[
                {name: 'Advance Tickets', value: 'advance_tickets'},
                {name: 'Only Pay at Door', value: 'only_pay_at_door'},
                {name: 'Not Required', value: 'not_required'}
              ]}
              label="Redemption"
              afterChange={({ value }) => this.setState({ redemption: value })}
            />
            {redemption === 'advance_tickets' ? (
              <Field
                name="eventbrite"
                component={EventbriteFinder}
                label="Eventbrite"
                placeholder="Select Event"
              />
            ) : null}
            {redemption === 'advance_tickets' ? (
              <Field
                name="event_url"
                component={renderField}
                label="Event Url"
                placeholder="Enter direct link to purchase online tickets (optional)"
              />
            ) : null}
            {redemption != 'advance_tickets' ? (
              <Field
                name="cost"
                component={TextCheckboxField}
                label="Event Cost"
                addon=".00"
              />
            ) : null}
            
          </div>
          <div className="col-md-1 help-tooltip" style={{ "padding-top": "30px" }}>
            <div className="col-md-12" style={{"margin-bottom" : "60px"}}>
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Dynamic base on selection"></p>
            </div>
            <div className="col-md-12" style={{"margin-bottom" : "170px"}}>
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Please enter the name and dates of each event occurrence."></p>
            </div>
            <div className="col-md-12" style={{"margin-bottom" : "45px"}} >
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Please enter how users can gain access to the event."></p>
            </div>
            {redemption != 'advance_tickets' ? (
              <div className="col-md-12">
                <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Please enter the average price of the event."></p>
              </div>  
            ): null}
            
            <ReactTooltip place="right" type="success" effect="solid"/>
          </div>
        </div>
        {errorMessage ? (
          <div className="alert alert-danger">
            <strong>Oops!</strong> {errorMessage}
          </div>
        ) : null}
        <div className="btn-group">
          <LinkTo className="btn btn-default" url="events">Cancel</LinkTo>
          <button action="submit" className="btn btn-primary">
            {isEmpty(item) ? 'Create Event' : 'Update Event'}
          </button>
        </div>
      </form>
    );
  }
}

EventForm.defaultProps = {
  item: {}
};

EventForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  item: PropTypes.shape({
    objectId: PropTypes.string
  })
};

export default connect(({
  auth: { currentUser },
  eventTypes: { items: eventTypes },
  specials: { items: specials },
  locations: { items: locations },
}) => ({ eventTypes, locations, specials, currentUser }), ({
  fetchEventTypes,
  fetchLocations,
  fetchSpecials
}))(reduxForm({ form: 'event' })(EventForm));
