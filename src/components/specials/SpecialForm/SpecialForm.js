import range from 'lodash/range';
import isEmpty from 'lodash/isEmpty';
import isObject from 'lodash/isObject';
import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Promise from 'bluebird';
import ReactTooltip from 'react-tooltip';

import { fetchLocations } from '../../../actions/LocationActions';

import {
  LinkTo,
  WeekdayStartEndList,
  renderField,
  renderFileUploadField,
  renderTextareaField,
  renderDropdownList,
  renderDatePicker,
  renderCheckboxField,
  renderTooltip,
} from '../../../helpers';

class SpecialForm extends Component {

  state = {};

  componentDidMount() {
    const { fetchLocations } = this.props;
    Promise.all([
      fetchLocations({})
    ]).then(() => this.handleInitialize());
  }

  handleInitialize() {
    const {
      item,
      item: {
        incentive_name, category, incentive_type, attendee_min, amount, item_name, description, location,
        redemption_options, promo_code, days, start_date, end_date, without_end_date, image
      },
      initialize
    } = this.props;

    if (!isEmpty(item)) {
      this.setState({
        without_end_date,
        category: isObject(category) ? category.value : null,
        incentive_type: isObject(incentive_type) ? incentive_type.value : null,
        redemption_options: isObject(redemption_options) ? redemption_options.value : null
      });
      initialize({
        incentive_name, category, incentive_type, attendee_min, amount, item_name, description, location,
        redemption_options, promo_code, days, start_date, end_date, without_end_date, image
      });
    }
  }

  render () {
    const { item, locations, errorMessage, handleSubmit, onSave } = this.props;
    const { category, incentive_type, redemption_options, without_end_date } = this.state;

    return (
      <form onSubmit={handleSubmit(special => onSave(special))}>
        <div className="row">
          <div className="col-md-6">
            <Field
              name="category"
              valueField="value"
              textField="name"
              component={renderDropdownList}
              data={[
                {name: 'Group Rate', value: 'group_rate'},
                {name: 'Special Event', value: 'special_event'},
                {name: 'Birthday', value: 'birthday'},
                {name: 'Happy Hour', value: 'happy_hour'},
                {name: 'Brunch', value: 'brunch'}
              ]}
              label="Category"
              placeholder="Select Category"
              afterChange={({ value }) => this.setState({ category: value })}
            />
            <Field
              name="incentive_name"
              component={renderField}
              label="Incentive Name"
            />
            <Field
              name="incentive_type"
              valueField="value"
              textField="name"
              component={renderDropdownList}
              data={[
                {name: 'Fixed Amount', value: 'fixed_amount'},
                {name: '% Discount', value: 'per_cent_discount'},
                {name: 'Free Item', value: 'free_item'},
                {name: 'VIP Benefits', value: 'vip_benefits'}
              ]}
              label="Incentive Type"
              placeholder="Select Incentive Type"
              afterChange={({ value }) => this.setState({ incentive_type: value })}
            />
            {category === 'group_rate' ? (
                <Field
                  name="attendee_min"
                  component={renderDropdownList}
                  data={range(2, 21)}
                  label="Attendee Minimum"
                />
              ) : null}
            {(incentive_type === 'vip_benefits') || (incentive_type === 'free_item') || (incentive_type === 'free_item') || (incentive_type === 'vip_benefits') ? null : (
                <Field
                  name="amount"
                  component={renderField}
                  type="number"
                  label="Amount"
                  placeholder="Enter Discount Amount"
                  prefix={incentive_type === 'fixed_amount' ? '$' : null}
                  addon={incentive_type === 'per_cent_discount' ? '% off' : null}
                />
              )}
            <Field 
              name="description" 
              component={renderTextareaField} 
              label="Description"
              placeholder="Example: Receive 50% OFF the entire transaction when you purchase 4 entrees."
             />
            <Field
              name="redemption_options"
              valueField="value"
              textField="name"
              component={renderDropdownList}
              data={incentive_type === 'vip_benefits' ? [{name: 'Mobile Image', value: 'mobile_image'}] : [
                {name: 'Mobile Image', value: 'mobile_image'},
                {name: 'Not Required', value: 'not_required'},
                {name: 'Promo Code', value: 'promo_code'}
              ]}
              label="Redemption Options"
              afterChange={({ value }) => this.setState({ redemption_options: value })}
            />
            <Field
              time
              name="days"
              component={WeekdayStartEndList}
              label="Start Time"
            />
            <Field
              name="start_date"
              component={renderDatePicker}
              label="Start Date"
              placeholder="Select Start Date"
            />
            <Field
              name="end_date"
              disabled={without_end_date}
              component={renderDatePicker}
              label="End Date"
              placeholder="Select End Date"
            />
            <Field
              name="without_end_date"
              component={renderCheckboxField}
              label="No End Date"
              afterChange={({ target: { checked } }) => this.setState({ without_end_date: checked })}
            />
          </div>
          <div className="col-md-1 help-tooltip" style={{ "padding-top": "30px" }}>
            <div className="col-md-12" style={{"margin-bottom" : "45px", "margin-top":"75px"}}>
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Must be less than 25 characters in length"></p>
            </div>
            
            <div className="col-md-12" style={{"margin-bottom" : "210px"}}>
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="What type of award will be granted?"></p>
            </div>
            <div className="col-md-12" style={{"margin-bottom" : "70px"}}>
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="What will be required of each user in order for the incentive or discount to be awarded?"></p>
            </div>
            <div className="col-md-12" style={{"margin-bottom" : "125px"}}>
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Indicate the date and time periods the incentive will be active for use. Select All Day and the incentive will remain active during the respective day's hours of operation."></p>
            </div>
            <div className="col-md-12">
              <p className="help-btn glyphicon glyphicon-info-sign" data-tip="Indicate the duration of the incentive. Select No End Date and the incentive will remain active indefinitely."></p>
            </div>
            <ReactTooltip place="right" type="success" effect="solid"/>
          </div>
          <div className="col-md-6">
            {redemption_options === 'mobile_image' ? (
              <Field name="image" component={renderFileUploadField} label="Image Upload" />
            ) : null}
            {redemption_options === 'promo_code' ? (
              <Field name="promo_code" component={renderField} label="Promo Code" />
            ) : null}
          </div>
        </div>
        {errorMessage ? (
            <div className="alert alert-danger">
              <strong>Oops!</strong> {errorMessage}
            </div>
          ) : null}
        <div className="btn-group">
          <LinkTo className="btn btn-default" url="specials">Cancel</LinkTo>
          <button action="submit" className="btn btn-primary">
            {isEmpty(item) ? 'Create Special' : 'Update Special'}
          </button>
        </div>
      </form>
    );
  }
}

SpecialForm.defaultProps = {
  item: {}
};

SpecialForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  item: PropTypes.shape({
    objectId: PropTypes.string
  })
};

export default connect(({
  locations: { items: locations },
}) => ({ locations }), ({ fetchLocations }))(reduxForm({ form: 'special' })(SpecialForm));