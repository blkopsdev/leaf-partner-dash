import React, { Component } from 'react';
import { connect } from 'react-redux';

import { signupUser } from '../../actions/AuthActions';

import { SignupForm } from '../../components';

class SignupPage extends Component {

  render() {
    const { errorMessage, signupUser } = this.props;
    return (
      <div className="middle-box text-center loginscreen animated fadeInDown">
        <img className="logo-name img-responsive" style={{"margin-left": "0"}} src={'https://s3-us-west-1.amazonaws.com/firstplace-4545/lander/assets/images/logo-green.png'} />
        <SignupForm 
          errorMessage={errorMessage}
          onSignup={user => signupUser(user)}
        />
      </div>
    );
  }
}

export default connect(({
  auth: {
    isAuthenticated,
    errorMessage
  }
}) => ({ isAuthenticated, errorMessage }), { signupUser })(SignupPage);
