import React, { Component } from 'react';
import { connect } from 'react-redux';

import { signinUser } from '../../actions/AuthActions';

import { SigninForm } from '../../components';

class LoginPage extends Component {

  render() {
    const { errorMessage, signinUser } = this.props;

    return (
      <div className="middle-box text-center loginscreen animated fadeInDown">
        <img className="logo-name img-responsive" style={{"margin-left": "0"}} src={'https://s3-us-west-1.amazonaws.com/firstplace-4545/lander/assets/images/logo-green.png'} />
        <SigninForm
          errorMessage={errorMessage}
          onSignin={({ email, password }) => signinUser({ email, password })}
        />
      </div>
    );
  }
}

export default connect(({
  auth: {
    isAuthenticated,
    errorMessage
  }
}) => ({ isAuthenticated, errorMessage }), { signinUser })(LoginPage);
